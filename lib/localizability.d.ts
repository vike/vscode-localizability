export interface LocalizeInfo {
    key: string;
    comment: string[];
}
interface LocalizeInfoFunc {
    (info: LocalizeInfo, message: string, ...args: (string | number | boolean | undefined | null)[]): string;
}
interface LocalizeStringFunc {
    (key: string, message: string, ...args: (string | number | boolean | undefined | null)[]): string;
}
export interface LocalizeFunc {
    (args: Parameters<LocalizeInfoFunc>): ReturnType<LocalizeInfoFunc>;
    (args: Parameters<LocalizeStringFunc>): ReturnType<LocalizeStringFunc>;
}
export declare type LocalizeParameters = Parameters<LocalizeInfoFunc> | Parameters<LocalizeStringFunc>; /** Dummy implementation to be settable to anything compatible with `require('vscode-nls').localize` @4.1.2
    * @example class Localizer{static localize=dummyLocalize};const localize=localizeBy(Localizer);console.log(localize('example', "E.g. {0}", 'any'))
    */
export declare function dummyLocalize(...parameters: LocalizeParameters): string;
export declare type Localizer = {
    localize: typeof dummyLocalize;
}; /** Factory for glue in case it's needed for https://www.npmjs.com/package/vscode-nls-dev undocumented beyond link in https://www.npmjs.com/package/vscode-nls where the former is said to extract usage of the latter, of which only exist examples of non-proprietary static function calls.
    * @see				   dummyLocalize
    */
export declare function localizeBy(localizer: Localizer): (...parameters: LocalizeParameters) => string;
export {};
//# sourceMappingURL=localizability.d.ts.map