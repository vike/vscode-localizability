"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
;
function dummyLocalize(...parameters) { return parameters[1].replace(/\{(0|[1-9][0-9]*)\}/g, (...m) => parameters[+m[1] + 2]); } // I dislike JS plus-op in that unordered set of types (lhs/rhs) has precedence over list order, assuming var-names mirror type, else prefix the empty string-literal by a plus for the rest to be concatenated, or both side operands must be prefixed with plus to force ints (and spacing to disambiguate from incrementation): `int+str` ≠ `str+int` ≠ `+str+int`, while the latter equals `int+ +str`; as I come to think, I have no obvious example of a language that combines plus-operator over types in a to me more intuitive way, since all languages on top of my head have different ops for string and numeric handling of operands
exports.dummyLocalize = dummyLocalize;
;
function localizeBy(localizer) {
    return (...parameters) => localizer
        .localize(...parameters);
}
exports.localizeBy = localizeBy;
//# sourceMappingURL=index.js.map